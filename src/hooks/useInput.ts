import { ChangeEvent, Dispatch, useState, useCallback, SetStateAction } from 'react'

type Handler = (e: ChangeEvent<HTMLInputElement>) => void
type ReturnTypes = [string, Handler, Dispatch<SetStateAction<string>>]
const useInput = (initValue: string): ReturnTypes => {
  const [value, setValue] = useState(initValue || '')
  const handler = useCallback((e) => {
    setValue(e.target.value)
  }, [])
  return [value, handler, setValue]
}

export default useInput
